'use strict';
const { sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {

    async create(ctx) {
        let entity;
        const {fechaRealizada, productos} = ctx.request.body;
        console.log({fechaRealizada, productos});
        entity = await strapi.services.venta.create({fechaRealizada});
        productos.map(async element => {
            await strapi.services.ventasproductos.create({
                "venta": entity.id,
                "producto": element.id,
                "cantidad": element.cant
            });
        });
        entity = await strapi.query('venta').findOne({id: entity.id}, ['ventasproductos']);
        console.log(entity);
        return sanitizeEntity(entity, { model: strapi.models.venta });;

    }

};
